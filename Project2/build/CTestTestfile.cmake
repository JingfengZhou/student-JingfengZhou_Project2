# CMake generated Testfile for 
# Source directory: /home/xavier/Documents/self-driving-car/Project2/src
# Build directory: /home/xavier/Documents/self-driving-car/Project2/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(learning_tf)
subdirs(using_markers)
